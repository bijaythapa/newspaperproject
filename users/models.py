from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    username = models.CharField(max_length=150, help_text="* (150 characters only)", unique=True)
    age = models.PositiveIntegerField(null=True, blank=True)