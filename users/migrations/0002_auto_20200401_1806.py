# Generated by Django 3.0.4 on 2020-04-01 18:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='username',
            field=models.CharField(help_text='* (150 characters only)', max_length=150, unique=True),
        ),
    ]
